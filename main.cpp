/*
 * Main.cpp
 *
 *  Parte 1 del trabajo grupal: Saúl Jou González, Guillermo García Mesa y Pelayo García Suárez.
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <immintrin.h> // Requerido para usar funciones intrínsecas

#define ITEMS_PER_PACKET (sizeof(__m256)/sizeof(float)) // Definición de una constante que indica cuántos datos (floats) tenemos en cada paquete de 256 bits

using namespace cimg_library;

// Tipos de datos para las componentes de la imagen
typedef double data_t;

const char* SOURCE_IMG1      = "bailarina.bmp";
const char* SOURCE_IMG2      = "background_V.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";


int main() {
	const int REP = 18; // Número de repeticiones de bucle
	
	// Comprobación de existencia de imágenes
	try
	{
		CImg<data_t> srcImage1(SOURCE_IMG1);
		CImg<data_t> srcImage2(SOURCE_IMG2);
	}
	catch(const std::exception& e)
	{
		printf("ERROR: No existen las imágenes solicitadas. Saliendo del programa...");
		return -1;
	}
	// Abrir archivo e inicialización de objetos
	CImg<data_t> srcImage1(SOURCE_IMG1);
	CImg<data_t> srcImage2(SOURCE_IMG2);

	data_t *pRsrc1, *pGsrc1, *pBsrc1; // Punteros a las componentes R, G y B de src1
	data_t *pRsrc2, *pGsrc2, *pBsrc2; // Punteros a las componentes R, G y B de src2
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Puntero a los nuevos píxeles de la imagen
	uint width, height; // Anchura y altura de la imagen
	uint nComp; // Número de componentes de la imagen

	// Preparación de variables para el algoritmo
	struct timespec tStart, tEnd; // Tiempo inicial y final
	double dElapsedTimeS; // Tiempo transcurrido

	// Comprobación de misma dimensión en las dos imágenes
	if (srcImage1.width() != srcImage2.width() || srcImage1.height() != srcImage2.height())
	{
		printf("ERROR: Las dimensiones de las dos imágenes no coinciden. Cerrando programa...");
		return -1;
	}

	srcImage1.display(); // Muestra la imagen fuente
	width  = srcImage1.width(); // Consiguiendo información desde la imagen fuente
	height = srcImage1.height();
	nComp  = srcImage1.spectrum(); // Número de componentes de la imagen fuente
				// Valores comunes para el espectro (número de componentes de imagen):
				//  Imágenes en blanco y negro = 1
				//	Imágenes de color normal = 3 (RGB)
				//  Imágenes de color especial = 4 (RGB y canal alpha/transparencia)
	srcImage2.display();

	// Asignar espacio de memoria para el destino de los componentes de la imagen
	pDstImage = (data_t *) _mm_malloc (width * height * nComp * sizeof(data_t), sizeof(__m256));
	if (pDstImage == NULL) {
		perror("Asignar imagen de destino");
		exit(-2);
	}

    // Establecer, usando extensiones del vector, el valor inicial -1 a pDstImage
    *(__m256 *) pDstImage = _mm256_set1_ps(-1);
    *(__m256 *)(pDstImage + ITEMS_PER_PACKET)     = _mm256_set1_ps(-1);
    *(__m256 *)(pDstImage + ITEMS_PER_PACKET * 2) = _mm256_set1_ps(-1);

    // Paquetes de 32 bytes (256 bits). Usados para almacenar datos de memoria alineados
    __m256 vRsrc1, vGsrc1, vBsrc1;
	__m256 vRsrc2, vGsrc2, vBsrc2;

    // Los datos de pRsrc1, pGsrc1, pBsrc1... no tienen que estar alineados en memoria a __m256 (32 bytes)
    // por lo tanto, usaremos variables intermedias para evadir errores de ejecución
    // Hacemos una carga desalineada de vRsrc1, vGsrc1...
    vRsrc1 = _mm256_loadu_ps(pRsrc1);
    vGsrc1 = _mm256_loadu_ps(pGsrc1);

	// Punteros a las matrices de componentes de la imagen fuente 
	pRsrc1 = srcImage1.data(); // pRcomp apunta a la matriz de componente R de src1
	pGsrc1 = pRsrc1 + height * width; // pGcomp apunta a la matriz de componente G de src1
	pBsrc1 = pGsrc1 + height * width; // pBcomp apunta a la matriz de componente B de src1

	pRsrc2 = srcImage2.data(); // pRcomp apunta a la matriz de componente R de src2
	pGsrc2 = pRsrc2 + height * width; // pGcomp apunta a la matriz de componente G de src2
	pBsrc2 = pGsrc2 + height * width; // pBcomp apunta a la matriz de componente B de src2

	// Punteros a las matrices RGB de la imagen de destino
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;


	// Medición de tiempo inicial
	printf("Ejecutando tarea    : ");
	fflush(stdout);

	if (clock_gettime(CLOCK_REALTIME, &tStart) != 0)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	// Algoritmo
	// Blend: Blacken Mode (Algoritmo Nº 12)
	for (uint j = 0; j < REP; j++) // 18 repeticiones con tal de aumentar el tiempo de ejecución del programa
	{
		for (uint i = 0; i < width * height; i++){
			// R
			if ((255 - ((256 * (255 - *(pRsrc2 + i))) / (*(pRsrc1 + i) + 1))) > 255) *(pRdest + i) = 255; // Si la componente se satura por encima de 255, asignar 255
			else if ((255 - ((256 * (255 - *(pRsrc2 + i))) / (*(pRsrc1 + i) + 1))) < 0) *(pRdest + i) = 0; // Si la componente se satura por encima de 0, asignar 0
			else *(pRdest + i) = (255 - ((256 * (255 - *(pRsrc2 + i))) / (*(pRsrc1 + i) + 1)));
			// G
			if ((255 - ((256 * (255 - *(pGsrc2 + i))) / (*(pGsrc1 + i) + 1))) > 255) *(pGdest + i) = 255; // Si la componente se satura por encima de 255, asignar 255
			else if ((255 - ((256 * (255 - *(pGsrc2 + i))) / (*(pGsrc1 + i) + 1))) < 0) *(pGdest + i) = 0; // Si la componente se satura por encima de 0, asignar 0
			else *(pGdest + i) = (255 - ((256 * (255 - *(pGsrc2 + i))) / (*(pGsrc1 + i) + 1)));
			// B
			if ((255 - ((256 * (255 - *(pBsrc2 + i))) / (*(pBsrc1 + i) + 1))) > 255) *(pBdest + i) = 255; // Si la componente se satura por encima de 255, asignar 255
			else if ((255 - ((256 * (255 - *(pBsrc2 + i))) / (*(pBsrc1 + i) + 1))) < 0) *(pBdest + i) = 0; // Si la componente se satura por encima de 0, asignar 0
			else *(pBdest + i) = (255 - ((256 * (255 - *(pBsrc2 + i))) / (*(pBsrc1 + i) + 1)));
		}
	}

	// Medición de tiempo final
	if (clock_gettime(CLOCK_REALTIME, &tEnd) != 0)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	printf("Terminado\n");

	// Medimos el tiempo transcurrido
	dElapsedTimeS = tEnd.tv_sec - tStart.tv_sec;
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Tiempo transcurrido    : %f s.\n", dElapsedTimeS);
		
	// Creamos un nuevo objeto imagen con los píxeles calculados
	// En caso de imágenes de color normal usar nComp=3,
	// En caso de usar imágenes en blanco y negro usar nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	_mm_free(pDstImage);

	return 0;
}
